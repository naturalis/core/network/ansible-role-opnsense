# ansible-role-opnsense

NOTE: this is not a functional role yet, just a place to store some
functionality that might be included in a future Ansible role for OPNsense.

## pf_exporter

To monitor the state table size we've manually installed a textfile exporter
for the pf firewall.

1. Install the `os-node_exporter` plugin (System > Firmware > Plugins).
1. Login on the firewall and become root with `sudo -s`.
1. Place the `files/pf_exporter.sh` and `files/sponge.sh` files in the
   `/usr/local/sbin` directory on the firewall.
1. Place the `files/actions_pf_exporter.conf` file in the
   `/usr/local/opnsense/service/conf/actions.d` directory.
1. Restart `configd` in order to load the new config: `service configd restart`
1. Run `configctl pf_exporter start` to test the configd action. This should
   update / create the `/var/tmp/node_exporter.pf.prom` metrics file.
1. If that works, configure a cron job in the OPNsense GUI (System > Settings >
   Cron), and choose the 'Export pf metrics' as Command.
