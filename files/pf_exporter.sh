#!/bin/sh

PFMEM=$(pfctl -sm)
PFINFO=$(pfctl -si)

metric=`echo "$PFMEM" | awk '/states/ {print $4}' | head -n 1` 
if [ ! -z $metric ]; then
  echo '# HELP pf_memory_limit_states Memory limit on number of states.'
  echo '# TYPE pf_memory_limit_states gauge'
  echo 'pf_memory_limit_states '$metric
fi

metric=`echo "$PFMEM" | awk '/src-nodes/ {print $4}' | head -n 1` 
if [ ! -z $metric ]; then
  echo '# HELP pf_memory_limit_srcnodes Memory limit on number of source nodes.'
  echo '# TYPE pf_memory_limit_srcnodes gauge'
  echo 'pf_memory_limit_srcnodes '$metric
fi

metric=`echo "$PFMEM" | awk '/frags/ {print $4}' | head -n 1` 
if [ ! -z $metric ]; then
  echo '# HELP pf_memory_limit_frags Memory limit on number of frags.'
  echo '# TYPE pf_memory_limit_frags gauge'
  echo 'pf_memory_limit_frags '$metric
fi

metric=`echo "$PFMEM" | awk '/table-entries/ {print $4}' | head -n 1` 
if [ ! -z $metric ]; then
  echo '# HELP pf_memory_limit_tableentries Memory limit on entries in the pf state table.'
  echo '# TYPE pf_memory_limit_tableentries gauge'
  echo 'pf_memory_limit_tableentries '$metric
fi

metric=`echo "$PFINFO" | awk '/current entries/ {print $3}' | head -n 1` 
if [ ! -z $metric ]; then
  echo '# HELP pf_filter_statetable_entries Number of entries in the pf state table.'
  echo '# TYPE pf_filter_statetable_entries gauge'
  echo 'pf_filter_statetable_entries '$metric
fi

metric=`echo "$PFINFO" | awk '/searches/ {print $2}' | head -n 1` 
if [ ! -z $metric ]; then
  echo '# HELP pf_filter_statetable_searches Number of searches on the pf state table.'
  echo '# TYPE pf_filter_statetable_searches counter'
  echo 'pf_filter_statetable_searches '$metric
fi

metric=`echo "$PFINFO" | awk '/inserts/ {print $2}' | head -n 1` 
if [ ! -z $metric ]; then
  echo '# HELP pf_filter_statetable_inserts Number of inserts into the pf state table.'
  echo '# TYPE pf_filter_statetable_inserts counter'
  echo 'pf_filter_statetable_inserts '$metric
fi

metric=`echo "$PFINFO" | awk '/removals/ {print $2}' | head -n 1` 
if [ ! -z $metric ]; then
  echo '# HELP pf_filter_statetable_removals Number of removals from the pf state table.'
  echo '# TYPE pf_filter_statetable_removals counter'
  echo 'pf_filter_statetable_removals '$metric
fi

# pf_filter_counters_match
# pf_filter_counters_badoffset
# pf_filter_counters_fragment
# pf_filter_counters_short
# pf_filter_counters_normalize
# pf_filter_counters_memory
# pf_filter_counters_badtimestamp
# pf_filter_counters_congestion
# pf_filter_counters_ipoption
# pf_filter_counters_protocksum
# pf_filter_counters_statemismatch
# pf_filter_counters_stateinsert
# pf_filter_counters_statelimit
# pf_filter_counters_srclimit
# pf_filter_counters_synproxy
# pf_filter_counters_mapfailed
